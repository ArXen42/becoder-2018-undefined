﻿using System;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Undefined.WebApp
{
	public class Program
	{
		public static void Main(String[] args)
		{
			BuildWebHost(args).Run();
		}

		private static IWebHost BuildWebHost(String[] args)
		{
			return WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.UseUrls("http://localhost:5000", "http://10.20.0.76:5000")
				.Build();
		}
	}
}