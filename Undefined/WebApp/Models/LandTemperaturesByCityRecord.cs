﻿using System;

namespace Undefined.WebApp.Models
{
	public class LandTemperaturesByCityRecord
	{
		public Int32 Id { get; set; }

		public DateTime Date { get; set; }

		public virtual City  City   { get; set; }
		public         Int32 CityId { get; set; }

		public Single LandAverageTemperature            { get; set; }
		public Single LandAverageTemperatureUnsertainty { get; set; }
	}
}