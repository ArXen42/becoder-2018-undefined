﻿using Microsoft.EntityFrameworkCore;

namespace Undefined.WebApp.Models
{
	public class TemperaturesDbContext : DbContext
	{
		public DbSet<City>                            Cities                           { get; set; }
		public DbSet<GlobalTemperaturesRecord>        GlobalTemperaturesRecords        { get; set; }
		public DbSet<LandTemperaturesByCityRecord>    LandTemperaturesByCityRecords    { get; set; }
		public DbSet<LandTemperaturesByCountryRecord> LandTemperaturesByCountryRecords { get; set; }
		public DbSet<LandTemperaturesByStateRecord>   LandTemperaturesByStateRecords   { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder
				.UseNpgsql(@"Server=10.20.0.76;Database=SuppliesDb;User Id=undefined");
//				.UseLazyLoading();
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder
				.Entity<City>()
				.Property(c => c.Id)
				.ValueGeneratedOnAdd();
			modelBuilder
				.Entity<City>()
				.HasAlternateKey(c => new {c.Name, c.Country, c.Latitude, c.Longtude});

			modelBuilder
				.Entity<LandTemperaturesByCityRecord>()
				.Property(r => r.Id)
				.ValueGeneratedOnAdd();
			modelBuilder
				.Entity<LandTemperaturesByCityRecord>()
				.HasAlternateKey(r => new {r.Date, r.CityId});

			modelBuilder
				.Entity<LandTemperaturesByCountryRecord>()
				.Property(r => r.Id)
				.ValueGeneratedOnAdd();
			modelBuilder
				.Entity<LandTemperaturesByCountryRecord>()
				.HasAlternateKey(r => new {r.Date, r.Country});

			modelBuilder
				.Entity<LandTemperaturesByStateRecord>()
				.Property(r => r.Id)
				.ValueGeneratedOnAdd();
			modelBuilder
				.Entity<LandTemperaturesByStateRecord>()
				.HasAlternateKey(r => new {r.Date, r.Country, r.State});
		}
	}
}