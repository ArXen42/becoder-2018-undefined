﻿using System;

namespace Undefined.WebApp.Models
{
	public class LandTemperaturesByStateRecord
	{
		public Int32 Id { get; set; }

		public DateTime Date    { get; set; }
		public String   Country { get; set; }
		public String   State   { get; set; }

		public Single LandAverageTemperature            { get; set; }
		public Single LandAverageTemperatureUnsertainty { get; set; }
	}
}