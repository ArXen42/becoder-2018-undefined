﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Undefined.WebApp.Models
{
	public class GlobalTemperaturesRecord
	{
		[Key]
		public DateTime Date { get; set; }

		public Single  LandAverageTemperature                    { get; set; }
		public Single  LandAverageTemperatureUnsertainty         { get; set; }
		public Single? LandMaxTemperature                        { get; set; }
		public Single? LandMaxTemperatureUnsertainty             { get; set; }
		public Single? LandMinTemperature                        { get; set; }
		public Single? LandMinTemperatureUnsertainty             { get; set; }
		public Single? LandAndOceanAverageTemperature            { get; set; }
		public Single? LandAndOceanAverageTemperatureUnsertainty { get; set; }
	}
}