﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace Undefined.WebApp.Models
{
	public static class CountriesAbbreviations
	{
		private struct Entry
		{
			public String Name { get; set; }
			public String Id   { get; set; }
		}

		public static readonly  Dictionary<String, String> Names;
		private static readonly Dictionary<String, String> Abbreviations;

		public static String GetAbbreviation(String name)
		{
			String result = null;
			Abbreviations.TryGetValue(name, out result);

			return result;
		}

		static CountriesAbbreviations()
		{
			String json = File.ReadAllText(@"Resources/ids.json");
			var    obj  = JsonConvert.DeserializeObject<List<Entry>>(json);

			Names         = obj.ToDictionary(entry => entry.Id,   entry => entry.Name);
			Abbreviations = obj.ToDictionary(entry => entry.Name, entry => entry.Id);
		}
	}
}