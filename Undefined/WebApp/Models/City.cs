﻿using System;

namespace Undefined.WebApp.Models
{
	public class City
	{
		public Int32 Id { get; set; }

		public String  Name     { get; set; }
		public String  Country  { get; set; }
		public Single  Latitude { get; set; }
		public Single  Longtude { get; set; }
		public Boolean IsMajor  { get; set; }
	}
}