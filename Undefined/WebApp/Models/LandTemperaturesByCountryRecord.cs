﻿using System;

namespace Undefined.WebApp.Models
{
	public class LandTemperaturesByCountryRecord
	{
		public Int32 Id { get; set; }

		public DateTime Date    { get; set; }
		public String   Country { get; set; }

		public Single LandAverageTemperature            { get; set; }
		public Single LandAverageTemperatureUnsertainty { get; set; }
	}
}