﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Undefined.WebApp.Models;

namespace Undefined.WebApp.Controllers
{
	[Produces("application/json")]
	[Route("api/[controller]")]
	public class TemperaturesController : ControllerBase
	{
		[HttpGet("cities")]
		[AllowCrossSiteJson]
		[ProducesResponseType(typeof(IEnumerable<City>), 200)]
		public ActionResult GetCities()
		{
			using (var context = new TemperaturesDbContext())
			{
				return Ok(context.Cities.ToList());
			}
		}

		[HttpGet("countries")]
		[ProducesResponseType(typeof(IEnumerable<String>), 200)]
		[AllowCrossSiteJson]
		public ActionResult GetCountries()
		{
			using (var context = new TemperaturesDbContext())
			{
				var countries = context.Cities
					.Select(city => city.Country)
					.Distinct()
					.ToArray()
					.Select(CountriesAbbreviations.GetAbbreviation);

				return Ok(countries.ToArray());
			}
		}

		[AllowCrossSiteJson]
		[HttpGet("countries/map")]
		public ActionResult GetLastYearEverageMap()
		{
			using (var context = new TemperaturesDbContext())
			{
				Int32 maxYear = context.LandTemperaturesByCountryRecords.Max(r => r.Date.Year);

				var grouped = context
					.LandTemperaturesByCountryRecords
					.Where(r => r.Date.Year > maxYear - 5)
					.GroupBy(r => r.Country)
					.Select(g => new {Id = CountriesAbbreviations.GetAbbreviation(g.Key), Avg = g.Average(r => r.LandAverageTemperature)});

				var filtered = grouped
					.ToList()
					.Where(g => g.Id != null);

				return Ok(filtered.ToArray());
			}
		}

		[HttpGet("global/lastyear")]
		[AllowCrossSiteJson]
		public ActionResult GetLastYearSeasonsData()
		{
			using (var context = new TemperaturesDbContext())
			{
				var latestDate = context.GlobalTemperaturesRecords.Max(r => r.Date);

				var lastYearData = context
					.GlobalTemperaturesRecords
					.Where(r => r.Date.Year == latestDate.Year)
					.ToList();

				Single season1Avg = lastYearData
					.Where(r => new[] {1, 2, 12}.Contains(r.Date.Month))
					.Average(r => r.LandAverageTemperature);

				Single season2Avg = lastYearData
					.Where(r => new[] {3, 4, 5}.Contains(r.Date.Month))
					.Average(r => r.LandAverageTemperature);

				Single season3Avg = lastYearData
					.Where(r => new[] {6, 7, 8}.Contains(r.Date.Month))
					.Average(r => r.LandAverageTemperature);

				Single season4Avg = lastYearData
					.Where(r => new[] {9, 10, 11}.Contains(r.Date.Month))
					.Average(r => r.LandAverageTemperature);

				Int32 spreadPositive = lastYearData
					.Count(r => r.LandAverageTemperature > 0);

				Int32 spreadNegative = lastYearData
					.Count(r => r.LandAverageTemperature <= 0);

				return Ok(new
				{
					LatestDate     = latestDate.ToShortDateString(),
					Seasons        = new[] {season1Avg, season2Avg, season3Avg, season4Avg},
					SpreadPositive = spreadPositive,
					SpreadNegative = spreadNegative
				});
			}
		}

		[HttpGet("global/period/{beginYear}/{endYear}")]
		[AllowCrossSiteJson]
		public ActionResult GetPeriodInfo(Int32 beginYear, Int32 endYear)
		{
			using (var context = new TemperaturesDbContext())
			{
				var resultData = context
					.GlobalTemperaturesRecords
					.Where(r => r.Date.Year >= beginYear && r.Date.Year <= endYear)
					.GroupBy(r => r.Date.Year)
					.Select(g => new
					{
						Year = g.Key,
						Min  = g.Min(r => r.LandAverageTemperature),
						Max  = g.Max(r => r.LandAverageTemperature),
						Avg  = g.Average(r => r.LandAverageTemperature),
					});

				return Ok(resultData.ToArray());
			}
		}
	}
}