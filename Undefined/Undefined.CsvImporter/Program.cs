﻿using System;
using System.IO;

namespace Undefined.CsvImporter
{
	internal class Program
	{
		private static void Main(String[] args)
		{
			var si = new DataImporter();
//			si.ReinitializeDatabase();

			Console.WriteLine("Importing global temperatures...");
			using (var fs = new FileStream("GlobalTemperatures.csv", FileMode.Open, FileAccess.Read))
			{
				si.ImportGlobalTemperatures(fs);
			}

			Console.WriteLine("Importing temperatures by country");
			using (var fs = new FileStream("GlobalLandTemperaturesByCountry.csv", FileMode.Open, FileAccess.Read))
			{
				si.ImportTemperaturesByCountry(fs);
			}

			Console.WriteLine("Importing temperatures by state");
			using (var fs = new FileStream("GlobalLandTemperaturesByState.csv", FileMode.Open, FileAccess.Read))
			{
				si.ImportTemperaturesByState(fs);
			}

			Console.WriteLine("Importing non-major cities...");
			using (var fs = new FileStream("GlobalLandTemperaturesByCity.csv", FileMode.Open, FileAccess.Read))
			{
				si.ImportCities(fs);
			}

			Console.WriteLine("Importing major cities...");
			using (var fs = new FileStream("GlobalLandTemperaturesByMajorCity.csv", FileMode.Open, FileAccess.Read))
			{
				si.ImportCitiesIsMajor(fs);
			}

			Console.WriteLine("Importing temperatures by city...");
			using (var fs = new FileStream("GlobalLandTemperaturesByCity.csv", FileMode.Open, FileAccess.Read))
			{
				si.ImportTemperaturesByCity(fs);
			}
		}
	}
}