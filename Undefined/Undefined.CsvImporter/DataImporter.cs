﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Undefined.WebApp.Models;

namespace Undefined.CsvImporter
{
	internal class DataImporter
	{
		private const Int32 RecordsPerTransaction = 50000;

		public void ImportGlobalTemperatures(Stream inputStream)
		{
			GlobalTemperaturesRecord ImportLine(String line)
			{
				var arr = line.Split(',');

				Single? ParseSingle(String str)
				{
					if (String.IsNullOrEmpty(str))
						return null;
					return Single.Parse(str, CultureInfo.InvariantCulture);
				}

				if (String.IsNullOrEmpty(arr[1]) || String.IsNullOrEmpty(arr[2]))
					return null;

				return new GlobalTemperaturesRecord
				{
					Date                                      = DateTime.ParseExact(arr[0], "yyyy-MM-dd", CultureInfo.InvariantCulture),
					LandAverageTemperature                    = Single.Parse(arr[1], CultureInfo.InvariantCulture),
					LandAverageTemperatureUnsertainty         = Single.Parse(arr[2], CultureInfo.InvariantCulture),
					LandMaxTemperature                        = ParseSingle(arr[3]),
					LandMaxTemperatureUnsertainty             = ParseSingle(arr[4]),
					LandMinTemperature                        = ParseSingle(arr[5]),
					LandMinTemperatureUnsertainty             = ParseSingle(arr[6]),
					LandAndOceanAverageTemperature            = ParseSingle(arr[7]),
					LandAndOceanAverageTemperatureUnsertainty = ParseSingle(arr[8])
				};
			}

			var lines = new List<String>();
			using (var reader = new StreamReader(inputStream))
			{
				reader.ReadLine(); //Для пропуска шапки с заголовками

				String line = reader.ReadLine();
				while (line != null)
				{
					lines.Add(line);
					line = reader.ReadLine();
				}
			}

			using (var context = new TemperaturesDbContext())
			{
				context.AddRange(lines
					.Select(ImportLine)
					.Where(r => r != null));
				context.SaveChanges();
			}
		}

		public void ImportCities(Stream inputStream)
		{
			using (var context = new TemperaturesDbContext())
			using (var reader = new StreamReader(inputStream))
			{
				reader.ReadLine(); //Для пропуска шапки с заголовками
				String lastCityName      = String.Empty;
				String lastCountryName   = String.Empty;
				String lastLatitudeStr   = String.Empty;
				String lastLongtitudeStr = String.Empty;

				Int32  cityNum = 0;
				String line    = reader.ReadLine();
				while (line != null)
				{
					var    arr           = line.Split(',');
					String cityName      = arr[3];
					String countryName   = arr[4];
					String latitudeStr   = arr[5];
					String longtitudeStr = arr[6];

					if (cityName         == lastCityName
					    && countryName   == lastCountryName
					    && latitudeStr   == lastLatitudeStr
					    && longtitudeStr == lastLongtitudeStr)
					{
						line = reader.ReadLine();
						continue;
					}

					Single latitude = latitudeStr[latitudeStr.Length - 1] == 'N'
						? Single.Parse(latitudeStr.Replace("N", String.Empty), CultureInfo.InvariantCulture)
						: -Single.Parse(latitudeStr.Replace("S", String.Empty), CultureInfo.InvariantCulture);

					Single longtitude = longtitudeStr[longtitudeStr.Length - 1] == 'E'
						? Single.Parse(longtitudeStr.Replace("E", String.Empty), CultureInfo.InvariantCulture)
						: -Single.Parse(longtitudeStr.Replace("W", String.Empty), CultureInfo.InvariantCulture);

					var city = new City
					{
						Name     = cityName,
						Country  = countryName,
						Latitude = latitude,
						Longtude = longtitude,
						IsMajor  = false
					};

					context.Cities.Add(city);

					if (++cityNum % 50 == 0)
						context.SaveChanges();

					Console.WriteLine($"Imported {cityName}");

					lastCityName      = cityName;
					lastCountryName   = countryName;
					lastLatitudeStr   = latitudeStr;
					lastLongtitudeStr = longtitudeStr;

					line = reader.ReadLine();
				}

				context.SaveChanges();
			}
		}

		public void ImportCitiesIsMajor(Stream inputStream)
		{
			using (var context = new TemperaturesDbContext())
			using (var reader = new StreamReader(inputStream))
			{
				reader.ReadLine(); //Для пропуска шапки с заголовками

				String lastCityName      = String.Empty;
				String lastCountryName   = String.Empty;
				String lastLatitudeStr   = String.Empty;
				String lastLongtitudeStr = String.Empty;
				String line              = reader.ReadLine();
				while (line != null)
				{
					var    arr           = line.Split(',');
					String cityName      = arr[3];
					String countryName   = arr[4];
					String latitudeStr   = arr[5];
					String longtitudeStr = arr[6];

					if (cityName         == lastCityName
					    && countryName   == lastCountryName
					    && latitudeStr   == lastLatitudeStr
					    && longtitudeStr == lastLongtitudeStr)
					{
						line = reader.ReadLine();
						continue;
					}


					Single latitude = latitudeStr[latitudeStr.Length - 1] == 'N'
						? Single.Parse(latitudeStr.Replace("N", String.Empty), CultureInfo.InvariantCulture)
						: -Single.Parse(latitudeStr.Replace("S", String.Empty), CultureInfo.InvariantCulture);

					Single longtitude = longtitudeStr[longtitudeStr.Length - 1] == 'E'
						? Single.Parse(longtitudeStr.Replace("E", String.Empty), CultureInfo.InvariantCulture)
						: -Single.Parse(longtitudeStr.Replace("W", String.Empty), CultureInfo.InvariantCulture);

					var city = context.Cities.First(c => c.Name                               == cityName
					                                     && c.Country                         == countryName
					                                     && Math.Abs(c.Latitude - latitude)   < 0.01f
					                                     && Math.Abs(c.Longtude - longtitude) < 0.01f);
					city.IsMajor = true;

					lastCityName      = cityName;
					lastCountryName   = countryName;
					lastLatitudeStr   = latitudeStr;
					lastLongtitudeStr = longtitudeStr;
					line              = reader.ReadLine();
				}

				context.SaveChanges();
			}
		}

		public void ImportTemperaturesByCity(Stream inputStream)
		{
			using (var reader = new StreamReader(inputStream))
			using (var context = new TemperaturesDbContext())
			{
				Dictionary<String, City> cities = context
					.Cities
					.ToDictionary(city => city.Name + city.Country + city.Latitude + city.Longtude);

				reader.ReadLine(); //Для пропуска шапки с заголовками

				Int32  recordNum = 0;
				String line      = reader.ReadLine();
				while (line != null)
				{
					var arr = line.Split(',');

					if (String.IsNullOrEmpty(arr[1]) || String.IsNullOrEmpty(arr[2]))
					{
						line = reader.ReadLine();
						continue;
					}

					String cityName      = arr[3];
					String countryName   = arr[4];
					String latitudeStr   = arr[5];
					String longtitudeStr = arr[6];

					Single latitude = latitudeStr[latitudeStr.Length - 1] == 'N'
						? Single.Parse(latitudeStr.Replace("N", String.Empty), CultureInfo.InvariantCulture)
						: -Single.Parse(latitudeStr.Replace("S", String.Empty), CultureInfo.InvariantCulture);

					Single longtitude = longtitudeStr[longtitudeStr.Length - 1] == 'E'
						? Single.Parse(longtitudeStr.Replace("E", String.Empty), CultureInfo.InvariantCulture)
						: -Single.Parse(longtitudeStr.Replace("W", String.Empty), CultureInfo.InvariantCulture);


					var city = cities[cityName + countryName + latitude + longtitude];

					var record = new LandTemperaturesByCityRecord
					{
						Date                              = DateTime.ParseExact(arr[0], "yyyy-MM-dd", CultureInfo.InvariantCulture),
						LandAverageTemperature            = Single.Parse(arr[1], CultureInfo.InvariantCulture),
						LandAverageTemperatureUnsertainty = Single.Parse(arr[2], CultureInfo.InvariantCulture),
						City                              = city,
						CityId                            = city.Id
					};

					try
					{
						context.LandTemperaturesByCityRecords.Add(record);

						if (++recordNum % RecordsPerTransaction == 0)
							context.SaveChanges();
					}
					catch (Exception ex)
					{
						Console.WriteLine(ex + $" record {line}");
						Console.WriteLine();
					}

					line = reader.ReadLine();
				}

				context.SaveChanges();
			}
		}

		public void ImportTemperaturesByCountry(Stream inputStream)
		{
			using (var reader = new StreamReader(inputStream))
			using (var context = new TemperaturesDbContext())
			{
				reader.ReadLine(); //Для пропуска шапки с заголовками

				Int32  recordNum = 0;
				String line      = reader.ReadLine();
				while (line != null)
				{
					var arr = line.Split(',');

					if (String.IsNullOrEmpty(arr[1]) || String.IsNullOrEmpty(arr[2]))
					{
						line = reader.ReadLine();
						continue;
					}

					var record = new LandTemperaturesByCountryRecord
					{
						Date                              = DateTime.ParseExact(arr[0], "yyyy-MM-dd", CultureInfo.InvariantCulture),
						LandAverageTemperature            = Single.Parse(arr[1], CultureInfo.InvariantCulture),
						LandAverageTemperatureUnsertainty = Single.Parse(arr[2], CultureInfo.InvariantCulture),
						Country                           = arr[3]
					};

					context.LandTemperaturesByCountryRecords.Add(record);

					if (++recordNum % RecordsPerTransaction == 0)
						context.SaveChanges();

					line = reader.ReadLine();
				}

				context.SaveChanges();
			}
		}

		public void ImportTemperaturesByState(Stream inputStream)
		{
			using (var reader = new StreamReader(inputStream))
			using (var context = new TemperaturesDbContext())
			{
				reader.ReadLine(); //Для пропуска шапки с заголовками

				Int32  recordNum = 0;
				String line      = reader.ReadLine();
				while (line != null)
				{
					var arr = line.Split(',');

					if (String.IsNullOrEmpty(arr[1]) || String.IsNullOrEmpty(arr[2]))
					{
						line = reader.ReadLine();
						continue;
					}

					var record = new LandTemperaturesByStateRecord
					{
						Date                              = DateTime.ParseExact(arr[0], "yyyy-MM-dd", CultureInfo.InvariantCulture),
						LandAverageTemperature            = Single.Parse(arr[1], CultureInfo.InvariantCulture),
						LandAverageTemperatureUnsertainty = Single.Parse(arr[2], CultureInfo.InvariantCulture),
						State                             = arr[3],
						Country                           = arr[4]
					};

					context.LandTemperaturesByStateRecords.Add(record);

					if (++recordNum % RecordsPerTransaction == 0)
						context.SaveChanges();

					line = reader.ReadLine();
				}

				context.SaveChanges();
			}
		}

		public void ReinitializeDatabase()
		{
			using (var context = new TemperaturesDbContext())
			{
				Console.WriteLine("Dropping database if exists...");
				context.Database.EnsureDeleted();
				Console.WriteLine("Creating fresh database...");
				context.Database.EnsureCreated();
				context.SaveChanges();
			}
		}
	}
}